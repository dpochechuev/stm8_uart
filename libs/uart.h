/**
  ******************************************************************************
  * @file    uart.h
  * @author  Dmitry Pochechuev pochechuev@gmail.com
  * @version V1.0.0
  * @date    19-June-2017  
  * @brief   This file contains all the prototypes/macros for the UART peripheral.
  */

#ifndef _UART_H
#define _UART_H

/* constants and macros */

/** @brief  UART Baudrate Expression
 *  @param  xtalCpu  system clock in Mhz, e.g. 4000000UL for 4Mhz          
 *  @param  baudRate baudrate in bps, e.g. 1200, 2400, 9600     
 */
#define UART_BAUD_SELECT(baudRate,xtalCpu) (xtalCpu/baudRate)

/** @brief  Size of the circular receive buffer, must be power of 2
 */
#ifndef UART_RX_BUFFER_SIZE
#define UART_RX_BUFFER_SIZE 32
#endif

/** @brief  Size of the circular transmit buffer, must be power of 2 
 */
#ifndef UART_TX_BUFFER_SIZE
#define UART_TX_BUFFER_SIZE 32
#endif

/* 
** high byte error return code of uart_getc()
*/

/** @brief Overrun condition by UART    */
#define UART_OVERRUN_ERROR    0x2000

/** @brief Noise detect RX line by UART */
#define UART_NOISE_DETECT     0x1000

/** @brief Framing Error by UART        */
#define UART_FRAME_ERROR      0x0800

/** @brief Parity Error by UART         */              
#define UART_PARITY_ERROR     0x0400

/** @brief receive ringbuffer overflow  */               
#define UART_BUFFER_OVERFLOW  0x0200

/** @brief no receive data available    */              
#define UART_NO_DATA          0x0100

/* function prototypes */

/**
   @brief   Initialize UART
   @param   Macro to calculate the division ratio 
   @return  none
*/
extern void uart_init(uint16_t);

/**
 *  @brief   Put byte to ringbuffer for transmitting via UART
 *  @param   data byte to be transmitted
 *  @return  none
 */
extern void uart_putc(uint8_t data);

/**
 *  @brief   Put string to ringbuffer for transmitting via UART
 *
 *  The string is buffered by the uart library in a circular buffer
 *  and one character at a time is transmitted to the UART using interrupts.
 *  Blocks if it can not write the whole string into the circular buffer.
 * 
 *  @param   s string to be transmitted
 *  @return  none
 */
extern void uart_puts(uint8_t *s );

/**
 *  @brief   Get received byte from ringbuffer
 *
 * Returns in the lower byte the received character and in the 
 * higher byte the last receive error.
 * UART_NO_DATA is returned when no data is available.
 *
 *  @return  lower byte:  received byte from ringbuffer
 *  @return  higher byte: last receive status
 *           - \b 0 successfully received data from UART
 *           - \b UART_NO_DATA           
 *             <br>no receive data available
 *           - \b UART_BUFFER_OVERFLOW   
 *             <br>Receive ringbuffer overflow.
 *             We are not reading the receive buffer fast enough, 
 *             one or more received character have been dropped 
 *           - \b UART_OVERRUN_ERROR     
 *             <br>Overrun condition by UART.
 *             A character already present in the UART UDR register was 
 *             not read by the interrupt handler before the next character arrived,
 *             one or more received characters have been dropped.
 *           - \b UART_FRAME_ERROR       
 *             <br>Framing Error by UART
 */
extern uint16_t uart_getc(void);   

#endif // _UART_H 