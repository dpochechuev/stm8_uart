/**
  ******************************************************************************
  * @file    uart.c
  * @author  Dmitry Pochechuev pochechuev@gmail.com
  * @version V1.0.0
  * @date    19-June-2017
  * @brief   This file contains all the functions/macros for the UART peripheral.
  ******************************************************************************
  */

/**
  * @mainpage STM8S UART library 
  * @note ���������� UART ��� ����������������� ��������� STM8S.
  * - ���������� ���������� ����������.
  *  - ���������� ���������� ������ ���� ��������� � �������� ���������.  
  * - ���������� ���������� ��������� ����� ��� ������ � ��������.
  *  - ������� ������� ������ ���� ������ 2! 16-32-64-128-256!
  *  - ������� ������� �� ������ ���� ������ 256! ��� ������� � ������(�������) 
  *    ������������ ����������(�������) ������������ uint8_t. 
  * - � ��������� uart_init() ������������ ��������� ������ GPIO.
  *  - ��� ������������ �������� �� STM8S003(103) ������ TSSOP20, ���������
  *    �������� ��������� ������ GPIO.
  * @author Dmitry Pochechuev pochechuev@gmail.com
  * @version V1.0.0
  * @date 19-June-2017 
  */


#include "stm8s.h"
#include "uart.h"

/* constants and macros */

/* size of RX/TX buffers */
#define UART_RX_BUFFER_MASK ( UART_RX_BUFFER_SIZE - 1)
#define UART_TX_BUFFER_MASK ( UART_TX_BUFFER_SIZE - 1)

#if ( UART_RX_BUFFER_SIZE & UART_RX_BUFFER_MASK )
#error RX buffer size is not a power of 2
#endif
#if ( UART_TX_BUFFER_SIZE & UART_TX_BUFFER_MASK )
#error TX buffer size is not a power of 2
#endif

/*
 *  module global variables
 */
static volatile uint8_t UART_TxBuf[UART_TX_BUFFER_SIZE];
static volatile uint8_t UART_RxBuf[UART_RX_BUFFER_SIZE];
static volatile uint8_t UART_TxHead;
static volatile uint8_t UART_TxTail;
static volatile uint8_t UART_RxHead;
static volatile uint8_t UART_RxTail;
static volatile uint8_t UART_LastRxError;

void uart_init(uint16_t cdiv)
{
	/* Configure TX Pin */
	/* Configure TX Pin tsop20 = PORT_D, PIN_5  */
	#ifdef STM8S003
	GPIOD->CR1 |= 0x20;
	GPIOD->ODR |= 0x20;
	GPIOD->DDR |= 0x20;
	GPIOD->CR2 |= 0x20;
	#endif
	
	UART_TxHead = 0;
    UART_TxTail = 0;
    UART_RxHead = 0;
    UART_RxTail = 0;
	UART_LastRxError = 0;

	UART1->BRR2 = (uint8_t)((cdiv & 0x000f) + ((cdiv >> 8) & 0x00f0));
	UART1->BRR1 = (uint8_t)((cdiv >> 4) & 0x00ff);
	
	UART1->CR2 |= (uint8_t)(UART1_CR2_RIEN | UART1_CR2_TEN | UART1_CR2_REN);
}

/*************************************************************************
Function: uart_getc()
Purpose:	return byte from ringbuffer  
Returns:	lower byte:  received byte from ringbuffer
		higher byte: last receive error
**************************************************************************/
uint16_t uart_getc(void)
{
	uint8_t tmptail;
	uint8_t data;
	uint8_t lastRxError;

	if ( UART_RxHead == UART_RxTail )
	{
		return UART_NO_DATA;   /* no data available */
	}
	/* calculate buffer index */
	tmptail = (UART_RxTail + 1) & UART_RX_BUFFER_MASK;
	/* get data from receive buffer */
	data = UART_RxBuf[tmptail];
	lastRxError = UART_LastRxError;
    /* store buffer index */
    UART_RxTail = tmptail;

    UART_LastRxError = 0;
    return (lastRxError << 8) + data;
}/* uart_getc */	


/*************************************************************************
Function: uart_putc()
Purpose:  write byte to ringbuffer for transmitting via UART
Input:    byte to be transmitted
Returns:  none          
**************************************************************************/
void uart_putc(uint8_t data)
{
    uint8_t tmphead;
    
    tmphead  = (UART_TxHead + 1) & UART_TX_BUFFER_MASK;
    
    while ( tmphead == UART_TxTail )
	{
		/* wait for free space in buffer */
    }
    
    UART_TxBuf[tmphead] = data;
    UART_TxHead = tmphead;
    /* enable TI interrupt */
	UART1->CR2 |= (uint8_t)(UART1_CR2_TIEN);
}/* uart_putc */

/*************************************************************************
Function: uart_puts()
Purpose:  transmit string to UART
Input:    string to be transmitted
Returns:  none          
**************************************************************************/
void uart_puts(uint8_t *s )
{
	while (*s)
	{		
		uart_putc(*s++);
	}
}/* uart_puts */

/**
  * @brief UART1 TX Interrupt routine.
  * @param  none
  * @retval none
  */
INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
{
	uint8_t tmptail;
	
	if ( UART_TxHead != UART_TxTail)
	{
		/* calculate and store new buffer index */
		tmptail = (UART_TxTail + 1) & UART_TX_BUFFER_MASK;
		UART_TxTail = tmptail;
		/* get one byte from buffer and write it to UART */
		UART1->DR = UART_TxBuf[tmptail];  /* start transmission */
	}
	else
	{
		/* tx buffer empty, disable TI interrupt */
		UART1->CR2 &= (uint8_t)(~UART1_CR2_TIEN);
	}
}

/**
  * @brief UART1 RX Interrupt routine.
  * @param  none
  * @retval none
  */
INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
{
	uint8_t tmphead;
	uint8_t data;
	uint8_t usr;
	uint8_t lastRxError;

	/* read UART status register and UART data register */
    usr  = UART1->SR;
	data = UART1->DR;
	/* Save UART errors */
	lastRxError = ((usr & (UART1_SR_OR | UART1_SR_NF | UART1_SR_FE | UART1_SR_PE))<<2);
    /* calculate buffer index */ 
    tmphead = ( UART_RxHead + 1) & UART_RX_BUFFER_MASK;
	if ( tmphead == UART_RxTail )
	{
		/* error: receive buffer overflow */
		lastRxError = UART_BUFFER_OVERFLOW >> 8;
	}
	else
	{
		/* store received data in buffer */
		UART_RxBuf[tmphead] = data;
		/* store new index */
		UART_RxHead = tmphead;
	}
	UART_LastRxError |= lastRxError; 	
}
