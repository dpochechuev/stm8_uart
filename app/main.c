#include "stm8s.h"
#include "uart.h"

#define F_CPU	16000000UL

/* define CPU frequency in Hz in Makefile */
#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

/* Define UART buad rate here */
#define UART_BAUD_RATE      19200      

uint16_t rx;

/* function prototypes */
void init(void);

int main( void )
{
  	init();
	enableInterrupts();
	uart_puts("UART_INT Hello"); 
	while(1)
	{
		rx = uart_getc();
		if(!(rx & UART_NO_DATA))
		{
            /*
             * new data available from UART
             * check for Frame or Overrun error
             */
            if ( rx & UART_FRAME_ERROR )
            {
                /* Framing Error detected, i.e no stop bit detected */
                uart_puts("UART Frame Error: ");
            }
            if ( rx & UART_OVERRUN_ERROR )
            {
                /* 
                 * Overrun, a character already present in the UART UDR register was 
                 * not read by the interrupt handler before the next character arrived,
                 * one or more received characters have been dropped
                 */
                uart_puts("UART Overrun Error: ");
            }
            if ( rx & UART_BUFFER_OVERFLOW )
            {
                /* 
                 * We are not reading the receive buffer fast enough,
                 * one or more received character have been dropped 
                 */
                uart_puts("Buffer overflow error: ");
            }
            /* 
             * send received character back
             */
            uart_putc( (uint8_t)rx );
		}
	}
}

void init(void)
{
	// Configure Clock
	CLK->CKDIVR = 0;
	// Configure UART
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU));
}
